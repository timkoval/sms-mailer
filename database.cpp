#include "database.h"

Database::Database()
{

}

QSqlTableModel* Database::getTable(QString tableName){
    QSqlTableModel *testDbModel = new QSqlTableModel;
    testDbModel->setTable(tableName);
    testDbModel->select();
    return testDbModel;
}

void Database::setDatabase(QString dbName, QString hostName, QString userName, QString password) {
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(hostName);
    db.setDatabaseName(dbName);
    db.setUserName(userName);
    db.setPassword(password);
    if (!db.open())
        qInfo("Connection failed");
    else
        qInfo("Connected sucsessfully");

}

void Database::uploadTable(QString path) {
    QSqlQuery uQuery;
    uQuery.exec("DELETE FROM test");
    uQuery.exec("COPY test FROM '" + path + "' WITH (FORMAT csv)");
}
