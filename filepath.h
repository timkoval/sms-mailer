#ifndef FILEPATH_H
#define FILEPATH_H

#include <QDialog>
#include <QFileDialog>
#include <database.h>
#include <QAbstractButton>

namespace Ui {
class FilePath;
}

class FilePath : public QDialog
{
    Q_OBJECT

public:
    explicit FilePath(QWidget *parent = nullptr);
    ~FilePath();

private slots:
    void on_pathButton_clicked();

    void on_uploadButton_clicked();

private:
    Ui::FilePath *ui;

    QString _path;
};

#endif // FILEPATH_H
