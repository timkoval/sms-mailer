#ifndef CONNECTION_H
#define CONNECTION_H

#include <QDialog>
#include <QSerialPort>
#include <QSerialPortInfo>

namespace Ui {
class Connection;
}

class Connection : public QDialog
{
    Q_OBJECT

public:
    explicit Connection(QWidget *parent = nullptr);
    ~Connection();

    QString showPorts();

private:
    Ui::Connection *ui;
};

#endif // CONNECTION_H
