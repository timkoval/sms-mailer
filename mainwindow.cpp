#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showTables(){
    Database mainDb;
    mainDb.setDatabase("clients", "127.0.0.1", "postgres", "yodick");
    ui->registryTable->setModel(mainDb.getTable("test"));
    ui->phonesTable->setModel(mainDb.getTable("test"));
}

void MainWindow::on_connectModem_triggered()
{
    Connection connectWindow;
    connectWindow.setModal(true);
    connectWindow.setWindowTitle("Подключение");
    _portNum = connectWindow.showPorts();
    connectWindow.exec();
}

void MainWindow::on_parseRegistry_triggered()
{
    FilePath filepathWindow;
    filepathWindow.setModal(true);
    filepathWindow.setWindowTitle("Загрузка файла реестра");
    filepathWindow.exec();
}

void MainWindow::on_mailing_triggered()
{
    QSerialPort currentPort;
    currentPort.setPortName(_portNum);

    for (int i = 0; i < ui->registryTable->model()->rowCount(); i++){
        for (int j = 0; j < ui->phonesTable->model()->rowCount(); j++){
            int regNum = ui->registryTable->model()->data(ui->registryTable->model()->index(i,0)).toInt();
            int phNum = ui->phonesTable->model()->data(ui->phonesTable->model()->index(j,0)).toInt();

            if (regNum == phNum) {
                QString phone = ui->phonesTable->model()->data(ui->phonesTable->model()->index(j,1)).toString();
                QString toRecieve = "AT" + phone;                           //TODO templates and AT command here
                QByteArray toRecieveBytes = toRecieve.toUtf8();

                if (currentPort.open(QIODevice::ReadWrite)) {
                    currentPort.write(toRecieveBytes);
                }
            }
        }
    }
}
