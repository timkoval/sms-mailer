#include "connection.h"
#include "ui_connection.h"

Connection::Connection(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Connection)
{
    ui->setupUi(this);
}

Connection::~Connection()
{
    delete ui;
}

QString Connection::showPorts() {
    foreach(const QSerialPortInfo &serialPorts, QSerialPortInfo::availablePorts()) {   //setting ports to ports box
        ui->ports->addItem(serialPorts.portName());
    }
    return ui->ports->currentText();
}

